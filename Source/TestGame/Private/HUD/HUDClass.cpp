// Fill out your copyright notice in the Description page of Project Settings.


#include "HUD/HUDClass.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Widgets/UserWidgetClass.h"

void AHUDClass::BeginPlay()
{

   Super::BeginPlay();
   if(Widget)
   {
     MyWidget = CreateWidget<UUserWidgetClass>(GetWorld(), Widget);

     if(MyWidget != nullptr)
     {
        MyWidget->AddToViewport();
     }
   }
}