// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/ControllableCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/CapsuleComponent.h"
#include "Math/UnrealMathUtility.h"


AControllableCharacter::AControllableCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	baseTurnRate = 45.0f;
	baseLookUpAtRate = 45.0f;
}


void AControllableCharacter::BeginPlay()
{
	Super::BeginPlay();
}


void AControllableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AControllableCharacter::MoveForward(float value)
{
    if((Controller) && (value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, value);
	}
}

void AControllableCharacter::MoveRight(float value)
{
	if((Controller) && (value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, value);
	}
}

void AControllableCharacter::Crouchs()
{
  this->GetCapsuleComponent()->SetCapsuleSize(34.f,45.f);
}

void AControllableCharacter::CrouchStop()
{
  this->GetCapsuleComponent()->SetCapsuleSize(34.f,80.f);
}

void AControllableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AControllableCharacter::Crouchs);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AControllableCharacter::CrouchStop);
	PlayerInputComponent->BindAxis("MoveForward", this, &AControllableCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AControllableCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Look", this, &APawn::AddControllerPitchInput);
}
