// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CharacterAbilitiesComponent.h"
#include "Components/AttackComponent.h"


APlayerCharacter::APlayerCharacter()
{
    CreateSubobject();
}

void APlayerCharacter::BeginPlay()
{
    Super::BeginPlay();
	StatusCharacter = EStatusCharacter::ESC_Walk;
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	StaminaChange(DeltaTime);
}

void APlayerCharacter::CreateSubobject()
{
    SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(RootComponent);
    
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent);

    CharacterAbilitiesPointer = CreateDefaultSubobject<UCharacterAbilitiesComponent>(TEXT("CharacterAbilities"));

	AttackComponent = CreateDefaultSubobject<UAttackComponent>(TEXT("AttackComponent"));
}

void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if(PlayerInputComponent)
	{
	  PlayerInputComponent->BindAction("Sprint", IE_Pressed, this,  &APlayerCharacter::Sprint);
	  PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::SprintStop);
	  PlayerInputComponent->BindAction("Attack", IE_Pressed, this,  &APlayerCharacter::Attack);
	}
}

void APlayerCharacter::Sprint()
{
  CameraComponent->SetFieldOfView(82.f);
  StatusCharacter = EStatusCharacter::ESC_Sprint;
}

void APlayerCharacter::Attack()
{
	AttackComponent->AttackMethod();
}

void APlayerCharacter::SprintStop()
{
  CameraComponent->SetFieldOfView(90.f);
  StatusCharacter = EStatusCharacter::ESC_Walk;
}

void APlayerCharacter::StaminaChange(float DeltaTime)
{
	if(CharacterAbilitiesPointer!=nullptr)
	{
		if(StatusCharacter == EStatusCharacter::ESC_Sprint && CharacterAbilitiesPointer->GetCurrentStamina()>0)
		{
           GetCharacterMovement()->MaxWalkSpeed=700.f;
		   CharacterAbilitiesPointer->ChangeStamina(DeltaTime * 100.f * (-0.20f));
		}else
		{
           GetCharacterMovement()->MaxWalkSpeed=420.f;
		   CharacterAbilitiesPointer->ChangeStamina(DeltaTime * 100.f * (0.20f));
		}
	}
}

