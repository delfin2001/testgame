// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/EnemyClass.h"
#include "Components/HealthComponent.h"
#include "Components/StaticMeshComponent.h"


AEnemyClass::AEnemyClass()
{
 	
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("Health"));
}


void AEnemyClass::BeginPlay()
{
	Super::BeginPlay();
	
}


void AEnemyClass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

