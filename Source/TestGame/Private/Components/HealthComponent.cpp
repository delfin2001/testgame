// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/HealthComponent.h"


UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	BasicHealth = 100.f;
	CurrentHealth = BasicHealth;
}



void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


float UHealthComponent::GetCurrentHealth()
{
   return  CurrentHealth;
}


float UHealthComponent::GetBasicHealth()
{
   return BasicHealth;
}

void UHealthComponent::SetHealth(float Damage)
{
   CurrentHealth = CurrentHealth + Damage; 
    if(CurrentHealth > BasicHealth)
	{
		CurrentHealth = BasicHealth;    
	}

	if(CurrentHealth < 0)
	{
		CurrentHealth = 0.f;
	}
}

