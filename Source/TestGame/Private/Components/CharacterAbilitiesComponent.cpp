// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/CharacterAbilitiesComponent.h"


UCharacterAbilitiesComponent::UCharacterAbilitiesComponent()
{
	
	PrimaryComponentTick.bCanEverTick = true;
    CurrentStaminaCharacter = 100.f;
	
}


// Called when the game starts
void UCharacterAbilitiesComponent::BeginPlay()
{
	Super::BeginPlay();

	
	
}



void UCharacterAbilitiesComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
}

void UCharacterAbilitiesComponent::ChangeStamina(float amountOfStamina)
{
    CurrentStaminaCharacter = CurrentStaminaCharacter + amountOfStamina; 
    if(CurrentStaminaCharacter > 100)
	{
		CurrentStaminaCharacter = 100.f;    
	}

	if(CurrentStaminaCharacter < 0)
	{
		CurrentStaminaCharacter = 0.f;
	}
}

float UCharacterAbilitiesComponent::GetCurrentStamina()
{
	return CurrentStaminaCharacter;
}
