// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/MoveObjectComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/Actor.h"
#include "Characters/PlayerCharacter.h"
#include "Math/Rotator.h"

UMoveObjectComponent::UMoveObjectComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UMoveObjectComponent::BeginPlay()
{
	Super::BeginPlay();
    ActorThatOpen = GetWorld()->GetFirstPlayerController()->GetPawn();
}



void UMoveObjectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if(PressurePlate)
      {
        if(PressurePlate->IsOverlappingActor(ActorThatOpen))
        {
          TestOverlapping(DeltaTime);
        }
      }   
          
}

void UMoveObjectComponent::TestOverlapping(float DeltaTime)
{
  if(MoveVector == EMoveVector::ESC_X)
  {
    float ActorLocation = GetOwner()->GetActorLocation().X;
    Actors_Location = GetOwner()->GetActorLocation();

    Actors_Location.X = FMath::FInterpTo
    (
        ActorLocation,
        Target,
        DeltaTime,
        MoveSpeed
    );
  }else if(MoveVector == EMoveVector::ESC_Y)
  {
     float ActorLocation = GetOwner()->GetActorLocation().Y;
    Actors_Location = GetOwner()->GetActorLocation();

    Actors_Location.Y = FMath::FInterpTo
    (
        ActorLocation,
        Target,
        DeltaTime,
        MoveSpeed
    );
  }else if(MoveVector == EMoveVector::ESC_Z)
  {
     float ActorLocation = GetOwner()->GetActorLocation().Z;
    Actors_Location = GetOwner()->GetActorLocation();

    Actors_Location.Z = FMath::FInterpTo
    (
        ActorLocation,
        Target,
        DeltaTime,
        MoveSpeed
    );
  }  
  ETeleportType Teleport = ETeleportType::None;
    GetOwner()->SetActorLocation
    (
        Actors_Location,
        false,
        OUT ActorHitted,
        Teleport
    );       
}

