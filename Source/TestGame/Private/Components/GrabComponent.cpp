// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GrabComponent.h"


UGrabComponent::UGrabComponent()
{

	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabComponent::BeginPlay()
{
	Super::BeginPlay();

}


void UGrabComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

