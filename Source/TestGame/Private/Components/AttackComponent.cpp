


#include "Components/AttackComponent.h"
#include "Characters/EnemyClass.h"
#include "Engine/World.h"
#include "CollisionQueryParams.h"
#include "DrawDebugHelpers.h"
#include "Components/HealthComponent.h"


UAttackComponent::UAttackComponent()
{
	
	PrimaryComponentTick.bCanEverTick = true;
    StandardPower = 100.f;
	 AttackPower = StandardPower;
	
}



void UAttackComponent::BeginPlay()
{
	Super::BeginPlay();
}


void UAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    UpdateAttackPower(DeltaTime * 1.2f * 100.f);
	
}

void UAttackComponent::UpdateAttackPower(float SetAttack)
{
    AttackPower += SetAttack;
	if(AttackPower>StandardPower)
	{
		AttackPower = StandardPower;
	}
}


void UAttackComponent::AttackMethod()
{
    if(GetAttackPower()==100.f)
	{

      FVector Loc;
      FRotator Rot;
      FHitResult Hit;

       GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(Loc, Rot);
       FVector Start = Loc;
       FVector End = Start + (Rot.Vector() * ReachInteract);

        FCollisionQueryParams TraceParams;
   //GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, TraceParams);
        GetWorld()->LineTraceSingleByObjectType(OUT Hit, Start, End, FCollisionObjectQueryParams(ECollisionChannel::ECC_WorldStatic), TraceParams);

        AActor* ActorReturned = Hit.GetActor();
	if(ActorReturned != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("1") );
	    EnemyCast = Cast<AEnemyClass>(ActorReturned);
		if(EnemyCast != nullptr)
		{
            UE_LOG(LogTemp, Warning, TEXT("2") );
           EnemyCast->HealthComp->SetHealth(-30.f);
		}
	}
	
   DrawDebugLine(GetWorld(), Start, End, FColor::Orange, false, 4.0f);
   AttackPower = 0.f;
	}
}

float UAttackComponent::GetAttackPower()
{
   return AttackPower;
}
