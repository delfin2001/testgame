// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterAbilitiesComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTGAME_API UCharacterAbilitiesComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterAbilitiesComponent();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CurrentStamina")
	float CurrentStaminaCharacter;

	UFUNCTION(BlueprintCallable)
	float GetCurrentStamina();

    UFUNCTION(BlueprintCallable)
	void ChangeStamina(float amountOfStamina);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
