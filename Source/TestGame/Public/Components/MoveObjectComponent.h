// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine.h"
#include "MoveObjectComponent.generated.h"	

class APlayerCharacter;
class ATriggerVolume;
class UCapsuleComponent;

UENUM(BlueprintType)
enum class EMoveVector : uint8
{
  ESC_X     UMETA(DisplayName = "X"),
  ESC_Y     UMETA(DisplayName = "Y"),
  ESC_Z    UMETA(DisplayName = "Z")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTGAME_API UMoveObjectComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMoveObjectComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	FVector Actors_Location;

public:	
	
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate;

	UPROPERTY(EditAnywhere)
	AActor* ActorThatOpen;

	UPROPERTY(EditAnywhere)
	UCapsuleComponent* Capsule;

	UPROPERTY(EditAnywhere)
    float Target = 0.f;

    UPROPERTY(EditAnywhere)
    float MoveSpeed = 0.4f;
	
	UPROPERTY(EditAnywhere, Category = "enums")
    EMoveVector MoveVector;

	FHitResult* ActorHitted = nullptr;

	void TestOverlapping(float DeltaTime);

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
