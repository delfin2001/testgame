// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttackComponent.generated.h"

class AEnemyClass;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTGAME_API UAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UAttackComponent();

	UPROPERTY(EditAnywhere)
	AEnemyClass* EnemyCast;

	UFUNCTION(BlueprintCallable, Category="AttackPower")
	void UpdateAttackPower(float SetAttack);

	UFUNCTION(BlueprintPure, Category="AttackPower")
	float GetAttackPower();

    UPROPERTY(EditAnywhere)
	float AttackPower;

    UPROPERTY(EditAnywhere)
	float StandardPower;

    UPROPERTY(EditAnywhere)
	float ReachInteract = 300.f;

	void AttackMethod();


protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
