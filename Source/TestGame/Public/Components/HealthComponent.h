// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTGAME_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UHealthComponent();

	UPROPERTY(EditAnywhere, Category="Health")
	float BasicHealth;
	
	UPROPERTY(EditAnywhere, Category="Health")
	float CurrentHealth;

    UFUNCTION(BlueprintCallable, Category="Health")
	void SetHealth(float Damage);
     
	UFUNCTION(BlueprintCallable, Category="Health")
	float GetCurrentHealth();

	UFUNCTION(BlueprintCallable, Category="Health")
	float GetBasicHealth();

	void DestroyActor();

protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
