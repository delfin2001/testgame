// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HUDClass.generated.h"

class UUserWidget;
class UUserWidgetClass;

UCLASS()
class TESTGAME_API AHUDClass : public AHUD
{
	GENERATED_BODY()
public:

	 UPROPERTY(EditDefaultsOnly, Category = "Widget")
     TSubclassOf<UUserWidget> Widget;

     UUserWidgetClass* MyWidget;

protected:

   virtual void BeginPlay() override;

	
};
