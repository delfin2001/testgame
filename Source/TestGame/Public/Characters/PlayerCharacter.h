// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/ControllableCharacter.h"
#include "PlayerCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UInputComponent;
class UCharacterAbilitiesComponent;
class UAttackComponent;

UCLASS()
class TESTGAME_API APlayerCharacter : public AControllableCharacter
{
	GENERATED_BODY()

public:
 
   APlayerCharacter();

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
   USpringArmComponent* SpringArmComponent;

   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
   UCameraComponent* CameraComponent;

   UPROPERTY(EditAnywhere)
   UAttackComponent* AttackComponent;

   UPROPERTY(VisibleAnywhere)
   UCharacterAbilitiesComponent* CharacterAbilitiesPointer;

   virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

   void Sprint();

	void SprintStop();

   void Attack();

   void StaminaChange(float DeltaTime);

   void CreateSubobject();

   virtual void BeginPlay() override;

   virtual void Tick(float DeltaTime) override;

	
};
