// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemyClass.generated.h"

class UHealthComponent;
class UStaticMeshComponent;

UCLASS()
class TESTGAME_API AEnemyClass : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AEnemyClass();

    UPROPERTY(VisibleAnywhere, Category = "StaticMesh")
	UStaticMeshComponent* StaticMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StaticMesh")
	UHealthComponent* HealthComp;

protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;

};
