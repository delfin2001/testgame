// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ControllableCharacter.generated.h"


UENUM(BlueprintType)
enum class EStatusCharacter : uint8
{
  ESC_Sprint     UMETA(DisplayName = "Sprint"),
  ESC_Walk      UMETA(DisplayName = "Walk"),
  ESC_Crouch     UMETA(DisplayName = "Crouch")
};


UCLASS()
class TESTGAME_API AControllableCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	AControllableCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EStatusCharacter StatusCharacter;

protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float baseTurnRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float baseLookUpAtRate;

	void MoveForward(float value);
	void MoveRight(float value);
	void TurnAtRate(float value);
	void LookUpAtRate(float value);
	void Crouchs();
	void CrouchStop();

public:	
	
	virtual void Tick(float DeltaTime) override;

	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
